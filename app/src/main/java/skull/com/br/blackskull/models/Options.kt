package skull.com.br.blackskull.models

import java.io.Serializable

data class Options (
        var title:String,
        var subtitle:String,
        var description:String,
        var background:Int
) : Serializable