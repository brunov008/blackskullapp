package skull.com.br.blackskull.view.activities.main

import android.content.Context
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import skull.com.br.blackskull.core.layer.service.BaseService
import skull.com.br.blackskull.models.DetranResponse
import skull.com.br.blackskull.models.ScanInfoResponse
import skull.com.br.blackskull.utils.AppUtil
import skull.com.br.blackskull.view.fragments.mainscan.CustomListener

/**
 * Created by Bruno on 17/02/2019.
 */
class MainService(context: Context) : BaseService(context) {

    fun getCarInfoInService(placa: String, listener: (DetranResponse.InfoResponse) -> Unit) {
        val params = HashMap<String, String>()
        params["placa"] = placa

        detranRepository.cardInfo(params).enqueue(object : Callback<DetranResponse> {

            override fun onFailure(call: Call<DetranResponse>?, t: Throwable?) {
                AppUtil.showToast(mContext, NOT_POSSIBLE_RECEIVE_DATA)
            }

            override fun onResponse(call: Call<DetranResponse>?, response: Response<DetranResponse>) {

                if (response.body() != null)
                    listener(response.body()!!.response)
                else
                    AppUtil.showToast(mContext, NOT_POSSIBLE_RECEIVE_DATA)
            }
        })
    }

    fun getOperationSystemInfoInService(ip:String, listener: CustomListener){
        val params = HashMap<String, String>()
        params["ip"] = ip
        params["option"] = "os"

        sendScanRequest(params, listener)
    }

    fun getPortsInfoInService(ip:String, listener: CustomListener){
        val params = HashMap<String, String>()
        params["ip"] = ip
        params["option"] = "port"

        sendScanRequest(params, listener)
    }

    private fun sendScanRequest(params : HashMap<String, String>, listener: CustomListener) {
        listener.showLoading()

        hackRepository.scanInfo(params).enqueue(object : Callback<ScanInfoResponse>{

            override fun onFailure(call: Call<ScanInfoResponse>, t: Throwable) {
                listener.stopLoading()
                AppUtil.showToast(mContext, NOT_POSSIBLE_RECEIVE_DATA)
            }

            override fun onResponse(call: Call<ScanInfoResponse>, response: Response<ScanInfoResponse>) {
                listener.stopLoading()
                if (response.body() != null)
                    listener.onResponseDelivered(response.body()!!.response)
                else
                    AppUtil.showToast(mContext, NOT_POSSIBLE_RECEIVE_DATA)
            }

        })
    }
}
