package skull.com.br.blackskull.utils.components.bottomsheet

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.View
import kotlinx.android.synthetic.main.bottom_sheet_ip_info.view.*
import skull.com.br.blackskull.R
import skull.com.br.blackskull.utils.IpUtils

class CustomIpInfoBottomSheet : BaseBottomSheet() {

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        val contentView = View.inflate(context, R.layout.bottom_sheet_ip_info, null)
        dialog.setContentView(contentView)

        contentView.wlan0.text = IpUtils.getMACAddress("wlan0")
        contentView.eth0.text = IpUtils.getMACAddress("eth0")
        contentView.ipv4.text = IpUtils.getIPAddress(true)
        contentView.ipv6.text = IpUtils.getIPAddress(false)

        configureStateChange(contentView)
    }
}