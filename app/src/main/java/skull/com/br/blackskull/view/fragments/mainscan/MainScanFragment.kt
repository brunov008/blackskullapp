package skull.com.br.blackskull.view.fragments.mainscan


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.fragment_main.view.*
import skull.com.br.blackskull.R
import skull.com.br.blackskull.core.enums.FragmentEnum
import skull.com.br.blackskull.core.layer.view.BaseFragment
import skull.com.br.blackskull.utils.components.bottomsheet.CustomScanInfoBottomSheet
import skull.com.br.blackskull.view.activities.main.MainActivity
import skull.com.br.blackskull.view.adapters.MainFragmentAdapter
import skull.com.br.blackskull.view.fragments.mainscan.ip.IpFragment
import skull.com.br.blackskull.view.fragments.mainscan.os.ScanOSFragment
import skull.com.br.blackskull.view.fragments.mainscan.ports.ScanPortsFragment

import java.util.*

class MainScanFragment : BaseFragment() {

    private lateinit var mainFragmentController: MainFragmentController

    private var hashMap: HashMap<FragmentEnum, CharSequence?>? = null

    companion object {

        private lateinit var fieldOsListener: () -> Unit

        private lateinit var fieldPortsListener: () -> Unit

        @JvmStatic fun setInvalidOsListener(listener: () -> Unit){
            fieldOsListener = listener
        }

        @JvmStatic fun setInvalidPortsListener(listener: () -> Unit){
            fieldPortsListener = listener
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_main, container, false)

        val mActivity = activity as MainActivity

        mainFragmentController = MainFragmentController(mActivity.mainController)

        setupViewpager(view)

        //listen text from ScanOSFragment
        val osObserver = Observer { _, update -> hashMap = update as HashMap<FragmentEnum, CharSequence?> }
        val observable = ScanOSFragment.getObservable()
        observable.addObserver(osObserver)

        //listen text from ScanPortsFragment
        val portsObserver = Observer { _, update -> hashMap = update as HashMap<FragmentEnum, CharSequence?> }
        val portsObservable = ScanPortsFragment.getObservable()
        portsObservable.addObserver(portsObserver)

        view.circularMainFragmentButton.setOnClickListener {
            mainFragmentController.resolveButton(hashMap, getListener(view, mActivity))
        }

        return view
    }

    private fun getListener(view: View, mActivity: MainActivity): CustomListener {
        return object : CustomListener{

            override fun invalidPortsField() {
                fieldPortsListener.invoke()
            }

            override fun invalidOsField() {
                fieldOsListener.invoke()
            }

            override fun showLoading() {
                view.circularMainFragmentButton.startAnimation()
            }

            override fun onResponseDelivered(response: String) {
                CustomScanInfoBottomSheet(response).show(mActivity.supportFragmentManager, "")
            }

            override fun stopLoading() {
                //val bitmap = BitmapFactory.decodeResource(context!!.resources, R.drawable.ic_cloud_upload_white_24dp)
                //view.circularMainFragmentButton.doneLoadingAnimation(R.color.white, bitmap)
                view.circularMainFragmentButton.revertAnimation()
            }
        }
    }

    private fun setupViewpager(view: View) {
        view.circularMainFragmentButton.visibility = View.INVISIBLE

        val adapter = MainFragmentAdapter(childFragmentManager)
        adapter.addFragment(IpFragment())
        adapter.addFragment(ScanOSFragment())
        adapter.addFragment(ScanPortsFragment())

        view.viewpager.offscreenPageLimit = 2
        view.viewpager.currentItem = 0
        view.viewpager.adapter = adapter
        view.tablayout.setupWithViewPager(view.viewpager)
        view.viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                if (position == 0)
                    view.circularMainFragmentButton.visibility = View.INVISIBLE
                else
                    view.circularMainFragmentButton.visibility = View.VISIBLE
            }

        })
    }
}
