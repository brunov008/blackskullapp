package skull.com.br.blackskull.core.enums

enum class FragmentEnum {
    SCANOS,
    SCANPORTS
}