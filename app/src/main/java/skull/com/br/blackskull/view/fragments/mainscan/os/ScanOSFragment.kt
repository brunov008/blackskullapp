package skull.com.br.blackskull.view.fragments.mainscan.os


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_scan_os.view.*
import skull.com.br.blackskull.R
import skull.com.br.blackskull.core.layer.view.BaseFragment
import skull.com.br.blackskull.core.observables.ScanOsObservable
import skull.com.br.blackskull.utils.MaskUtil
import skull.com.br.blackskull.view.fragments.mainscan.MainScanFragment

class ScanOSFragment : BaseFragment(){

    companion object {

        @JvmStatic
        private val observable = ScanOsObservable()

        fun getObservable(): ScanOsObservable {
            return observable
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_scan_os, container, false)

        observable.add("")

        val watcher = MaskUtil(view.editextOs, MaskUtil.IP_FORMAT) { atualString ->
            observable.add(atualString)
        }

        MainScanFragment.setInvalidOsListener {
            view.textInputLayoutOs.error =  context!!.getString(R.string.invalid_ip)
        }

        view.editextOs.addTextChangedListener(watcher)

        view.editextOs.setOnClickListener {
            view.textInputLayoutOs.isErrorEnabled = true
            view.textInputLayoutOs.error = null
            view.textInputLayoutOs.isErrorEnabled = false
        }

        return view
    }
}
