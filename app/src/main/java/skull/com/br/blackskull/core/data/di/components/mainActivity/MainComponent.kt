package skull.com.br.blackskull.core.data.di.components.mainActivity

import dagger.Component
import skull.com.br.blackskull.core.data.di.components.application.NetComponent
import skull.com.br.blackskull.core.data.di.modules.MainActivity.MainModule
import skull.com.br.blackskull.core.data.di.scopes.PerActivity
import skull.com.br.blackskull.view.activities.main.MainActivity

/**
 * Created by Bruno on 17/02/2019.
 */

@PerActivity
@Component(dependencies = [NetComponent::class], modules = [MainModule::class])
interface MainComponent {
    fun inject(mainActivity: MainActivity)
}