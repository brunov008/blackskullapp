package skull.com.br.blackskull.view.fragments.mainscan.ip


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_ip.view.*
import skull.com.br.blackskull.R
import skull.com.br.blackskull.core.layer.view.BaseFragment
import skull.com.br.blackskull.utils.components.bottomsheet.CustomIpInfoBottomSheet
import skull.com.br.blackskull.view.activities.main.MainActivity

class IpFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_ip, container, false)

        val mActivity = activity as MainActivity

        view.ipInformations.setOnClickListener {
            CustomIpInfoBottomSheet().show(mActivity.supportFragmentManager, "")
        }

        return view
    }
}
