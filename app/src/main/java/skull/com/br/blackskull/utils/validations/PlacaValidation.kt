package skull.com.br.blackskull.utils.validations

class PlacaValidation {

    companion object {
        fun validate(s: String): Boolean {
            return if (!(s.isEmpty() && s.length != 8)) {
                val regex = Regex("(([A-Z]){3})-(([0-9]){4})")
                s.matches(regex)
            } else {
                false
            }
        }
    }
}