package skull.com.br.blackskull.core.layer.controller

import skull.com.br.blackskull.core.layer.service.BaseService

/**
 * Created by Bruno on 16/02/2019.
 */
open class BaseController<out T : BaseService>(service: T) {
    val mService = service
    val mContext = service.mContext
}