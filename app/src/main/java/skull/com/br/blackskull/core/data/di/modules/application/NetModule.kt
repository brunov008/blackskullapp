package skull.com.br.blackskull.core.data.di.modules.application

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import skull.com.br.blackskull.core.application.DataConstants
import skull.com.br.blackskull.core.data.DataManager
import skull.com.br.blackskull.core.data.SharedPreferencesHelper
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by Bruno on 17/02/2019.
 */

@Module
class NetModule {

    private var SKULL_PREFERENCES: String = "skull_preferences"

    @Singleton
    @Provides
    fun provideDatamanager(application: Application, sharedPreferences: SharedPreferences): DataManager {
        val preferencesHelper = SharedPreferencesHelper(sharedPreferences)
        return DataManager(application, preferencesHelper)
    }

    @Singleton
    @Provides
    fun provideSharedPreferences(application: Application):SharedPreferences{
        return application.getSharedPreferences(SKULL_PREFERENCES, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder().excludeFieldsWithoutExposeAnnotation()
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun provideOkHttpCache(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024 // 10 MiB
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun providesLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache, sharedPreferences: SharedPreferences, context: Application, httpLogingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val client = OkHttpClient.Builder()
        client.addNetworkInterceptor(httpLogingInterceptor)
        client.cache(cache)
        client.readTimeout(2, TimeUnit.MINUTES)
        client.connectTimeout(2, TimeUnit.MINUTES)
        client.addInterceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()

            val url = originalHttpUrl.newBuilder()
                    .build()

            val requestBuilder = original.newBuilder()
//                    .addHeader("x-api-id", context.getString(R.string.x_app_id))
//                    .addHeader("x-api-key", context.getString(R.string.x_app_key))
                    .url(url)

            /*
            val cookie = sharedPreferences.getString(context.getString(R.string.cookie), "")

            if (!TextUtils.isEmpty(cookie)) {
                requestBuilder.addHeader(context.getString(R.string.cookie), cookie)

            }
            */

            val request = requestBuilder.build()
            val response = chain.proceed(request)

            response
        }
        return client.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(DataConstants.BASE_URL)
                .client(okHttpClient)
                .build()
    }
}