package skull.com.br.blackskull.view.fragments.carinfo

import skull.com.br.blackskull.models.DetranResponse
import skull.com.br.blackskull.view.activities.main.MainController

/**
 * Created by Bruno on 21/02/2019.
 */
class CarInfoController(private val mainController: MainController) {

    fun resolveButton(placa: String, listener: (DetranResponse.InfoResponse) -> Unit) {
        mainController.getCarInfo(placa, listener)
    }
}