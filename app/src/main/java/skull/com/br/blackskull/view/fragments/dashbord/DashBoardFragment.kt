package skull.com.br.blackskull.view.fragments.dashbord


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_dash_board.view.*
import skull.com.br.blackskull.R
import skull.com.br.blackskull.core.layer.view.BaseFragment
import skull.com.br.blackskull.models.Options
import skull.com.br.blackskull.utils.AppUtil
import skull.com.br.blackskull.view.adapters.RecyclerViewAdapter


class DashBoardFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_dash_board, container, false)

        configureRecyclerView(view)

        return view
    }

    private fun configureRecyclerView(view: View) {
        view.recyclerView.setHasFixedSize(true)
        view.recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        val list = ArrayList<Options>()
        val option1 = Options("Kali Linux", "subtitle", "dwadx", R.color.lightOrange)
        val option2 = Options("Scan", "subtitle", "dsax", R.color.lightOrange2)
        val option3 = Options("Informacoes sobre o Ip", "subtitle", "xawdwa", R.color.lightOrange3)
        val option4 = Options("Informacoes de Carros", "subtitle", "dasdw", R.color.lightOrange4)

        list.add(option1)
        list.add(option2)
        list.add(option3)
        list.add(option4)

        view.recyclerView.adapter = RecyclerViewAdapter(list){
            position -> AppUtil.showToast(context, position.toString())
        }

    }

}
