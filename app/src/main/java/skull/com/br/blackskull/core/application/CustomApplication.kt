package skull.com.br.blackskull.core.application

import android.app.Application
import skull.com.br.blackskull.core.data.di.components.application.*
import skull.com.br.blackskull.core.data.di.modules.application.AppModule
import skull.com.br.blackskull.core.data.di.modules.application.NetModule
import skull.com.br.blackskull.core.data.di.modules.application.RepositoryModule

/**
 * Created by Bruno on 16/02/2019.
 */
class CustomApplication : Application() {

    companion object {

        @JvmStatic
        lateinit var instance: CustomApplication

        @JvmStatic
        lateinit var netComponent: NetComponent

        @JvmStatic
        lateinit var repositoryComponent: RepositoryComponent

    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        netComponent = DaggerNetComponent.builder()
                .appModule(AppModule(this))
                .netModule(NetModule())
                .build()

        repositoryComponent = DaggerRepositoryComponent.builder()
                .netComponent(netComponent)
                .repositoryModule(RepositoryModule())
                .build()
    }
}