package skull.com.br.blackskull.core.observables

import skull.com.br.blackskull.core.enums.FragmentEnum

class ScanPortsObservable : IpTextObservable(){

    private val h = HashMap<FragmentEnum, CharSequence?>()

    fun add(c:CharSequence?){
        h[FragmentEnum.SCANPORTS] = c
        notify(h)
    }

    override fun notify(h:HashMap<FragmentEnum, CharSequence?>) {
        setChanged()
        notifyObservers(h)
    }
}