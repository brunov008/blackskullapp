package skull.com.br.blackskull

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import skull.com.br.blackskull.core.layer.view.BaseActivity
import skull.com.br.blackskull.view.activities.main.MainActivity

class InitializeActivity : BaseActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initialize)

        Handler().postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }, 2500)
    }
}
