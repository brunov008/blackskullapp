package skull.com.br.blackskull.core.layer.view

import androidx.appcompat.app.AppCompatActivity


/**
 * Created by Bruno on 17/02/2019.
 */
open class BaseActivity : AppCompatActivity()

