package skull.com.br.blackskull.models

import com.google.gson.annotations.Expose
import java.io.Serializable

data class ScanInfoResponse(@Expose val response:String) :Serializable