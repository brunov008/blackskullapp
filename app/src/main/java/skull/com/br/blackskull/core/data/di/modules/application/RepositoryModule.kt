package skull.com.br.blackskull.core.data.di.modules.application

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import skull.com.br.blackskull.core.application.retrofit.repository.DetranRepository
import skull.com.br.blackskull.core.application.retrofit.repository.HackRepository
import skull.com.br.blackskull.core.data.di.scopes.PerActivity

/**
 * Created by Bruno on 18/02/2019.
 */

@Module
class RepositoryModule {

    @Provides
    @PerActivity
    fun provideDetranRepository(retrofit: Retrofit) = retrofit.create(DetranRepository::class.java)

    @Provides
    @PerActivity
    fun provideHackRepository(retrofit: Retrofit) = retrofit.create(HackRepository::class.java)
}