package skull.com.br.blackskull.core.data

import android.content.Context
import skull.com.br.blackskull.core.data.di.scopes.ApplicationContext

/**
 * Created by Bruno on 16/02/2019.
 */

class DataManager(@ApplicationContext applicationContext: Context, val preferencesHelper: SharedPreferencesHelper) {

    //var database = database //TODO

    //private var context = applicationContext Contexto da aplicaçao

    fun saveAccessToken(value: String) {
        preferencesHelper.put(value)
    }

    fun getAccessToken(): String {
        return preferencesHelper.get("")
    }

}