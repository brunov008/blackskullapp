package skull.com.br.blackskull.view.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * Created by Bruno on 22/02/2019.
 */
class MainFragmentAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    private val list = ArrayList<Fragment>(3)

    override fun getItem(position: Int): Fragment {
        return list[position]
    }

    override fun getCount(): Int {
        return list.size
    }

    fun addFragment(fragment:Fragment) {
        list.add(fragment)
    }
}