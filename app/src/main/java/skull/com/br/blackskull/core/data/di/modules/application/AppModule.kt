package skull.com.br.blackskull.core.data.di.modules.application

import android.app.Application
import dagger.Module
import dagger.Provides
import skull.com.br.blackskull.core.application.CustomApplication
import javax.inject.Singleton

/**
 * Created by Bruno on 16/02/2019.
 */

@Module
class AppModule(var application: CustomApplication) {

    @Provides
    @Singleton
    fun providesApplication(): Application {
        return application
    }
}