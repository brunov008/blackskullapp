package skull.com.br.blackskull.core.data.di.components.application

import android.content.SharedPreferences
import com.google.gson.Gson
import dagger.Component
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import skull.com.br.blackskull.core.data.DataManager
import skull.com.br.blackskull.core.data.di.modules.application.NetModule
import skull.com.br.blackskull.core.data.di.modules.application.AppModule
import javax.inject.Singleton

/**
 * Created by Bruno on 17/02/2019.
 */

@Singleton
@Component(modules = [AppModule::class, NetModule::class])
interface NetComponent {
    fun datamanager():DataManager
    fun sharedPreferences(): SharedPreferences
    fun retrofit(): Retrofit
    fun okHttpClient(): OkHttpClient
    fun gson(): Gson
}