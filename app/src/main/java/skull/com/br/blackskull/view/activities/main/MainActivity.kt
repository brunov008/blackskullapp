package skull.com.br.blackskull.view.activities.main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import skull.com.br.blackskull.R
import skull.com.br.blackskull.core.application.CustomApplication
import skull.com.br.blackskull.core.data.di.components.mainActivity.DaggerMainComponent
import skull.com.br.blackskull.core.data.di.modules.MainActivity.MainModule
import skull.com.br.blackskull.core.layer.view.BaseActivity
import skull.com.br.blackskull.view.fragments.dashbord.DashBoardFragment
import javax.inject.Inject

class MainActivity : BaseActivity(){

    @Inject
    lateinit var mainController: MainController

    private var toggle: ActionBarDrawerToggle? = null

    init {
        DaggerMainComponent.builder()
                .mainModule(MainModule(this))
                .netComponent(CustomApplication.netComponent)
                .build()
                .inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        configureDrawer()

        mainController.showFragment(DashBoardFragment(), true)
    }

    private fun configureDrawer() {
        navigationView.setNavigationItemSelectedListener { item ->
            drawer.closeDrawer(GravityCompat.START)
            mainController.onNavigationItemSelected(item)
        }
        toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawerOpen, R.string.drawerClose)
        drawer.addDrawerListener(toggle!!)
        toggle!!.syncState()
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START)
        else
            super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (toggle!!.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}

