package skull.com.br.blackskull.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell.view.*
import skull.com.br.blackskull.R
import skull.com.br.blackskull.models.Options

class RecyclerViewAdapter(private val list: ArrayList<Options>, private val listener : (Int) -> Unit) : RecyclerView.Adapter<RecyclerViewAdapter.CustomViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        context = parent.context

        val view = LayoutInflater.from(context).inflate(R.layout.cell, parent, false)
        return CustomViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val option = list[position]

        holder.container.setBackgroundColor(ContextCompat.getColor(context, option.background))

        holder.title.text = option.title
        holder.subtitle.text = option.subtitle
        holder.description.text = option.description
        holder.view.setOnClickListener {
            listener(position)
        }
    }

    class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.title
        val subtitle = itemView.subtitle
        val description = itemView.description
        val container = itemView.mainContainer
        val view = itemView
    }
}