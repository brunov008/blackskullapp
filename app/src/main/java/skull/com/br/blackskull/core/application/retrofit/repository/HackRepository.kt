package skull.com.br.blackskull.core.application.retrofit.repository

import retrofit2.Call
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import skull.com.br.blackskull.models.ScanInfoResponse

/**
 * Created by Bruno on 18/02/2019.
 */
interface HackRepository {

    @FormUrlEncoded
    @POST("hack/exec")
    fun scanInfo(@FieldMap hashMap: HashMap<String, String>): Call<ScanInfoResponse>
}