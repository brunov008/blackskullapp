package skull.com.br.blackskull.utils.validations

class IpValidation {

    companion object {
        fun validate(s: String): Boolean {
            return if (!(s.isEmpty() && s.length != 13)) {
                val regex = Regex("(([0-9]){3}).(([0-9]){3}).(([0-9]){2}).(([0-9]){2})")
                s.matches(regex)
            } else {
                false
            }
        }
    }
}