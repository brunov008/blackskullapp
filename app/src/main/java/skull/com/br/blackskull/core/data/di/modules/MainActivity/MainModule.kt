package skull.com.br.blackskull.core.data.di.modules.MainActivity

import android.content.Context
import dagger.Module
import dagger.Provides
import skull.com.br.blackskull.core.data.DataManager
import skull.com.br.blackskull.core.data.di.scopes.PerActivity
import skull.com.br.blackskull.view.activities.main.MainController
import skull.com.br.blackskull.view.activities.main.MainService

/**
 * Created by Bruno on 17/02/2019.
 */

@Module
class MainModule(val context: Context) {

    @Provides
    @PerActivity
    fun provideController(dataManager: DataManager, mainService: MainService): MainController {
        return MainController(dataManager, mainService)
    }

    @Provides
    @PerActivity
    fun provideService(): MainService {
        return MainService(context)
    }
}