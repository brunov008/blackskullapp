package skull.com.br.blackskull.view.fragments.carinfo


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_car_info.view.*
import skull.com.br.blackskull.R
import skull.com.br.blackskull.core.layer.view.BaseFragment
import skull.com.br.blackskull.utils.MaskUtil
import skull.com.br.blackskull.utils.components.bottomsheet.CustomCarInfoBottomSheet
import skull.com.br.blackskull.utils.validations.PlacaValidation
import skull.com.br.blackskull.view.activities.main.MainActivity

class CarInfoFragment : BaseFragment() {

    private lateinit var carInfoController: CarInfoController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_car_info, container, false)

        val mActivity = activity as MainActivity

        carInfoController = CarInfoController(mActivity.mainController)

        val watcher = MaskUtil(view.edPlaca, MaskUtil.PLACA_FORMAT)

        view.edPlaca.addTextChangedListener(watcher)

        view.edPlaca.setOnClickListener {
            view.inputLayoutPlaca.isErrorEnabled = true
            view.inputLayoutPlaca.error = null
            view.inputLayoutPlaca.isErrorEnabled = false
        }

        view.circularButton.setOnClickListener {
            val text = view.edPlaca.text.toString()
            if (PlacaValidation.validate(text))
                carInfoController.resolveButton(text) {
                    carInfo -> CustomCarInfoBottomSheet(carInfo).show(mActivity.supportFragmentManager, "")
                }
            else
                view.inputLayoutPlaca.error = "Placa inválida"
        }

        return view
    }
}
