package skull.com.br.blackskull.utils

import android.content.Context
import android.widget.Toast

/**
 * Created by Bruno on 21/02/2019.
 */
class AppUtil {

    companion object {
        fun showToast(context: Context?, message: String) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }
}