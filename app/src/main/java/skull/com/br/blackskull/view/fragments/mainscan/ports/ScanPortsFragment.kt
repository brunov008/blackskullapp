package skull.com.br.blackskull.view.fragments.mainscan.ports


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_scan_ports.view.*
import skull.com.br.blackskull.R
import skull.com.br.blackskull.core.layer.view.BaseFragment
import skull.com.br.blackskull.core.observables.ScanPortsObservable
import skull.com.br.blackskull.utils.MaskUtil
import skull.com.br.blackskull.view.fragments.mainscan.MainScanFragment

class ScanPortsFragment : BaseFragment() {

    companion object {

        @JvmStatic
        private val observable = ScanPortsObservable()

        fun getObservable(): ScanPortsObservable {
            return observable
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_scan_ports, container, false)

        observable.add("")

        val watcher = MaskUtil(view.editextPorts, MaskUtil.IP_FORMAT) { atualString ->
            observable.add(atualString)
        }

        MainScanFragment.setInvalidPortsListener {
            view.textInputLayoutPorts.error = context!!.getString(R.string.invalid_ip)
        }

        view.editextPorts.addTextChangedListener(watcher)

        view.editextPorts.setOnClickListener{
            view.textInputLayoutPorts.isErrorEnabled = true
            view.textInputLayoutPorts.error = null
            view.textInputLayoutPorts.isErrorEnabled = false
        }

        return view
    }
}
