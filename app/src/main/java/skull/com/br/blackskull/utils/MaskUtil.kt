package skull.com.br.blackskull.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText


class MaskUtil(private val field: EditText,private val mask: String) : TextWatcher {

    private var listener: ((String) -> Unit)? = null

    constructor(field: EditText, mask:String, listener: ((String) -> Unit)?) : this(field, mask){
        this.listener = listener
    }

    companion object {
        const val IP_FORMAT = "###.###.##.##"
        const val PLACA_FORMAT = "###-####"
    }

    var isUpdating = false
    var old = ""

    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        init(s.toString())
    }

    private fun init(s: String) {
        val str = unmask(s)
        var mascara = ""
        if (isUpdating) {
            old = str
            isUpdating = false
            return
        }
        var i = 0

        run loop@{
            mask.forEach { m ->
                if (m != '#' && str.length > old.length) {
                    mascara += m
                    return@forEach //continue
                }
                try {
                    mascara += str[i]
                } catch (e: Exception) {
                    return@loop //break
                }
                i++
            }
        }
        isUpdating = true
        if(mask.equals(PLACA_FORMAT))
            field.setText(mascara.toUpperCase())
        else
            field.setText(mascara)

        field.setSelection(mascara.length)

        if (listener != null){
            listener!!.invoke(mascara)
        }
    }

    private fun unmask(s: String): String {
        return s.replace("[.]".toRegex(), "")
                .replace("[-]".toRegex(), "")
                .replace("[/]".toRegex(), "")
                .replace("[(]".toRegex(), "")
                .replace("[ ]".toRegex(), "")
                .replace("[:]".toRegex(), "")
                .replace("[)]".toRegex(), "")
    }
}