package skull.com.br.blackskull.core.data

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Bruno on 17/02/2019.
 */

class SharedPreferencesHelper (var sharedPreferences: SharedPreferences) {

    private var PREF_KEY_ACCESS_TOKEN: String = "access-token"

    fun put(value: String) {
        sharedPreferences.edit().putString(PREF_KEY_ACCESS_TOKEN, value).apply()
    }

    fun get(defaultValue: String): String {
        return sharedPreferences.getString(PREF_KEY_ACCESS_TOKEN, defaultValue)
    }
}