package skull.com.br.blackskull.core.layer.service

import android.content.Context
import skull.com.br.blackskull.R
import skull.com.br.blackskull.core.application.CustomApplication
import skull.com.br.blackskull.core.application.retrofit.repository.DetranRepository
import skull.com.br.blackskull.core.application.retrofit.repository.HackRepository
import javax.inject.Inject

/**
 * Created by Bruno on 16/02/2019.
 */
open class BaseService(val mContext: Context) {

    @Inject
    lateinit var detranRepository: DetranRepository

    @Inject
    lateinit var hackRepository: HackRepository

    companion object {
        const val NOT_POSSIBLE_RECEIVE_DATA = "Não foi possível recuperar os dados"
    }

    init {
        CustomApplication.repositoryComponent.inject(this)
    }
}