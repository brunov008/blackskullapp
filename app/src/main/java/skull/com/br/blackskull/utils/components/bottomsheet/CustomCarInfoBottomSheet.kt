package skull.com.br.blackskull.utils.components.bottomsheet

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.View
import kotlinx.android.synthetic.main.bottom_sheet_car_info.view.*
import skull.com.br.blackskull.R
import skull.com.br.blackskull.models.DetranResponse

@SuppressLint("ValidFragment")
class CustomCarInfoBottomSheet(private val info: DetranResponse.InfoResponse) : BaseBottomSheet() {

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        val contentView = View.inflate(context, R.layout.bottom_sheet_car_info, null)
        dialog.setContentView(contentView)

        contentView.bottomSheetCodigoRetorno.text = info.codigoRetorno
        contentView.bottomSheetMensagemRetorno.text = info.mensagemRetorno
        contentView.bottomSheetCodigoSituacao.text = info.codigoSituacao
        contentView.bottomSheetSituacao.text = info.situacao
        contentView.bottomSheetModelo.text = info.modelo
        contentView.bottomSheetMarca.text = info.marca
        contentView.bottomSheetCor.text = info.cor
        contentView.bottomSheetAno.text = info.ano
        contentView.bottomSheetAnoModelo.text = info.anoModelo
        contentView.bottomSheetPlaca.text = info.placa
        contentView.bottomSheetUF.text = info.uf
        contentView.bottomSheetMunicipio.text = info.municipio
        contentView.bottomSheetChassi.text = info.chassi
        contentView.bottomSheetDataInfo.text = info.dataAtualizacaoCaracteristicasVeiculo
        contentView.bottomSheetDataRoubo.text = info.dataAtualizacaoRouboFurto
        contentView.bottomSheetDataAtualizacao.text = info.dataAtualizacaoAlarme

        configureStateChange(contentView)
    }
}