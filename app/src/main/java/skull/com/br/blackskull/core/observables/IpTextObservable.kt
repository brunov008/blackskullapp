package skull.com.br.blackskull.core.observables

import skull.com.br.blackskull.core.enums.FragmentEnum
import java.util.*
import kotlin.collections.HashMap

abstract class IpTextObservable:Observable(){
    abstract fun notify(h:HashMap<FragmentEnum, CharSequence?>)
}