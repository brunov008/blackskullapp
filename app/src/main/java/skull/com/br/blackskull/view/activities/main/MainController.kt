package skull.com.br.blackskull.view.activities.main

import android.view.MenuItem
import androidx.fragment.app.Fragment
import skull.com.br.blackskull.R
import skull.com.br.blackskull.core.data.DataManager
import skull.com.br.blackskull.core.layer.controller.BaseController
import skull.com.br.blackskull.models.DetranResponse
import skull.com.br.blackskull.models.ScanInfoResponse
import skull.com.br.blackskull.view.fragments.carinfo.CarInfoFragment
import skull.com.br.blackskull.view.fragments.dashbord.DashBoardFragment
import skull.com.br.blackskull.view.fragments.mainscan.CustomListener
import skull.com.br.blackskull.view.fragments.mainscan.MainScanFragment

/**
 * Created by Bruno on 17/02/2019.
 */
class MainController(private val dataManager: DataManager, service: MainService) : BaseController<MainService>(service) {

    private val activity = mContext as MainActivity

    fun onNavigationItemSelected(item: MenuItem): Boolean {
        val fragment: Fragment

        when (item.itemId) {
            R.id.db -> {
                fragment = DashBoardFragment()
                showFragment(fragment, false)
            }
            R.id.scan -> {
                fragment = MainScanFragment()
                showFragment(fragment, false)
            }
            R.id.cars -> {
                fragment = CarInfoFragment()
                showFragment(fragment, false)
            }
            R.id.logout -> {
                activity.finishAffinity()
            }
        }
        return true
    }

    fun getPortsInfo(ip: String, listener: CustomListener) {
        mService.getPortsInfoInService(ip, listener)
    }

    fun getOperationSystemInfo(ip: String, listener: CustomListener) {
        mService.getOperationSystemInfoInService(ip, listener)
    }

    fun getCarInfo(placa: String, listener: (DetranResponse.InfoResponse) -> Unit) {
        mService.getCarInfoInService(placa, listener)
    }

    fun showFragment(fragment: Fragment, isFirstTime: Boolean) {

        when (fragment) {
            is DashBoardFragment -> changeToolbarText(mContext.getString(R.string.drawer_menu_dashbord))
            is MainScanFragment -> changeToolbarText(mContext.getString(R.string.drawer_scan))
            is CarInfoFragment -> changeToolbarText(mContext.getString(R.string.drawer_menu_cars))
            else -> changeToolbarText(mContext.getString(R.string.app_name))
        }

        if (isFirstTime) {
            activity.supportFragmentManager.beginTransaction().add(R.id.fragment_container, fragment).commit()
        } else {
            activity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit()
        }
    }

    private fun changeToolbarText(s: String) {
        activity.supportActionBar!!.title = s
    }
}