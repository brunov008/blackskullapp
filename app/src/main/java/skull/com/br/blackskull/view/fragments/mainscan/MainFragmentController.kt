package skull.com.br.blackskull.view.fragments.mainscan

import skull.com.br.blackskull.core.enums.FragmentEnum
import skull.com.br.blackskull.utils.AppUtil
import skull.com.br.blackskull.utils.validations.IpValidation
import skull.com.br.blackskull.view.activities.main.MainController

class MainFragmentController(private val mainController: MainController) {

    fun resolveButton(hashMap: HashMap<FragmentEnum, CharSequence?>?, listener: CustomListener) {
        if (hashMap != null) {
            if (hashMap.containsKey(FragmentEnum.SCANOS)) {
                val ip = hashMap[FragmentEnum.SCANOS].toString()
                if (validate(ip, listener)) {
                    mainController.getOperationSystemInfo(ip, listener)
                } else
                    listener.invalidOsField()
            } else if (hashMap.containsKey(FragmentEnum.SCANPORTS)) {
                val ip = hashMap[FragmentEnum.SCANPORTS].toString()
                if (validate(hashMap[FragmentEnum.SCANPORTS].toString(), listener)) {
                    mainController.getPortsInfo(ip, listener)
                } else
                    listener.invalidPortsField()
            }
        }
    }

    private fun validate(ip: String, listener: CustomListener): Boolean {
        return if (IpValidation.validate(ip))
            true
        else {
            listener.stopLoading()
            false
        }
    }
}