package skull.com.br.blackskull.core.data.di.components.application

import dagger.Component
import skull.com.br.blackskull.core.data.di.modules.application.RepositoryModule
import skull.com.br.blackskull.core.data.di.scopes.PerActivity
import skull.com.br.blackskull.core.layer.service.BaseService

/**
 * Created by Bruno on 18/02/2019.
 */

@PerActivity
@Component(dependencies = [NetComponent::class], modules = [RepositoryModule::class])
interface RepositoryComponent {
    fun inject(service: BaseService)
}