package skull.com.br.blackskull.view.fragments.mainscan

interface CustomListener {
    fun showLoading()
    fun onResponseDelivered(response: String)
    fun stopLoading()
    fun invalidOsField()
    fun invalidPortsField()
}