package skull.com.br.blackskull.models

import com.google.gson.annotations.Expose
import java.io.Serializable

/**
 * Created by Bruno on 18/02/2019.
 */
data class DetranResponse(@Expose val response: InfoResponse) :Serializable {

    data class InfoResponse(
        @Expose val codigoRetorno: String,
        @Expose val mensagemRetorno:String,
        @Expose val codigoSituacao:String,
        @Expose val situacao:String,
        @Expose val modelo:String,
        @Expose val marca:String,
        @Expose val cor:String,
        @Expose val ano:String,
        @Expose val anoModelo:String,
        @Expose val placa:String,
        @Expose val uf:String,
        @Expose val municipio:String,
        @Expose val chassi:String,
        @Expose val dataAtualizacaoCaracteristicasVeiculo:String,
        @Expose val dataAtualizacaoRouboFurto:String,
        @Expose val dataAtualizacaoAlarme:String
    ):Serializable
}