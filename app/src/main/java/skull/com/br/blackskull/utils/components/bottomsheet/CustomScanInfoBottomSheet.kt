package skull.com.br.blackskull.utils.components.bottomsheet

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.View
import kotlinx.android.synthetic.main.bottom_sheet_scan_info.view.*
import skull.com.br.blackskull.R

@SuppressLint("ValidFragment")
class CustomScanInfoBottomSheet(private val info: String): BaseBottomSheet(){

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        val contentView = View.inflate(context, R.layout.bottom_sheet_scan_info, null)
        dialog.setContentView(contentView)

        contentView.bottomSheetText.text = info

        configureStateChange(contentView)
    }

}